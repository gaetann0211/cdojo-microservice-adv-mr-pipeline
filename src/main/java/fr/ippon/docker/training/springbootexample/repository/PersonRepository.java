package fr.ippon.docker.training.springbootexample.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.ippon.docker.training.springbootexample.domain.Person;

/**
 * PersonRepository
 */
public interface PersonRepository extends JpaRepository<Person, Long> {

    
}