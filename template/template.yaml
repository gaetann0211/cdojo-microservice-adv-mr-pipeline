apiVersion: v1
kind: Template
metadata:
  name: app-template
parameters:
  -
    name: APPLICATION_NAME
    description: Name of the application
    required: true
    displayName: Application Name
  -
    name: MYSQL_USER
    displayName: MySQL Connection Username
    description: Username for MySQL user that will be used for accessing the database.
    value: bdduser
    required: true
  -
    name: MYSQL_PASSWORD
    displayName: MySQL Connection Password
    description: Password for the MySQL connection user.
    generate: expression
    from: "[a-zA-Z0-9]{16}"
    required: true
  -
    name: MYSQL_ROOT_PASSWORD
    displayName: MySQL Root Connection Password
    description: Password for the MySQL connection user.
    generate: expression
    from: "[a-zA-Z0-9]{16}"
    required: true
  -
    name: MYSQL_DATABASE
    displayName: MySQL Database name
    value: appbdd
    required: true

objects:
  -
    apiVersion: v1
    kind: BuildConfig
    metadata:
      labels:
        app: ${APPLICATION_NAME}
      name: ${APPLICATION_NAME}
    spec:
      failedBuildsHistoryLimit: 5
      nodeSelector: null
      output:
        to:
          kind: ImageStreamTag
          name: ${APPLICATION_NAME}:latest
      postCommit: {}
      resources: {}
      runPolicy: Serial
      source:
        binary: {}
        type: Binary
      strategy:
        type: Docker
      successfulBuildsHistoryLimit: 5
  -
    apiVersion: v1
    kind: ImageStream
    metadata:
      labels:
        app: ${APPLICATION_NAME}
      name: ${APPLICATION_NAME}
  -
    apiVersion: v1
    kind: Secret
    metadata:
      name: ${APPLICATION_NAME}-mysql
      labels:
        app: ${APPLICATION_NAME}
    stringData:
      database-user: "${MYSQL_USER}"
      database-password: "${MYSQL_PASSWORD}"
      database-root-password: "${MYSQL_ROOT_PASSWORD}"
      database-name: "${MYSQL_DATABASE}"
  -
    apiVersion: v1
    kind: DeploymentConfig
    metadata:
      labels:
        app: ${APPLICATION_NAME}
      name: ${APPLICATION_NAME}-mysql
    spec:
      replicas: 1
      selector:
        name: ${APPLICATION_NAME}-mysql
      strategy:
        activeDeadlineSeconds: 21600
        recreateParams:
          timeoutSeconds: 600
        resources: {}
        type: Recreate
      template:
        metadata:
          creationTimestamp: null
          labels:
            name: ${APPLICATION_NAME}-mysql
            app: ${APPLICATION_NAME}
        spec:
          containers:
          - name: ${APPLICATION_NAME}-mysql
            image: centos/mysql-57-centos7
            env:
              - name: MYSQL_USER
                valueFrom:
                  secretKeyRef:
                    key: database-user
                    name: ${APPLICATION_NAME}-mysql
              - name: MYSQL_PASSWORD
                valueFrom:
                  secretKeyRef:
                    key: database-password
                    name: ${APPLICATION_NAME}-mysql
              - name: MYSQL_ROOT_PASSWORD
                valueFrom:
                  secretKeyRef:
                    key: database-root-password
                    name: ${APPLICATION_NAME}-mysql
              - name: MYSQL_DATABASE
                valueFrom:
                  secretKeyRef:
                    key: database-name
                    name: ${APPLICATION_NAME}-mysql
          ports:
            - containerPort: 3306
              protocol: TCP
  -
    apiVersion: v1
    kind: Service
    metadata:
      name: ${APPLICATION_NAME}-mysql
      labels:
        app: ${APPLICATION_NAME}
    spec:
      ports:
        -
          name: mysql-bc
          protocol: TCP
          port: 3306
          targetPort: 3306
      selector:
        name: ${APPLICATION_NAME}-mysql
  -      
    apiVersion: v1
    kind: DeploymentConfig
    metadata:
      name: ${APPLICATION_NAME}
      labels:
        app: ${APPLICATION_NAME}
    spec:
      strategy:
        type: Rolling
        rollingParams:
          updatePeriodSeconds: 1
          intervalSeconds: 1
          timeoutSeconds: 600
          maxUnavailable: 25%
          maxSurge: 25%
          pre:
            failurePolicy: ignore
            execNewPod:
              command:
                - "/bin/sh"
                - "-c"
                - |
                    while true
                    do
                      rt=$(timeout 1 bash -c 'cat < /dev/null > /dev/tcp/${APPLICATION_NAME}-mysql/3306')
                      if [ $? -eq 0 ]; then
                        echo "DB is UP"
                        break
                      fi
                      echo "DB is not yet reachable;sleep for 10s before retry"
                      sleep 10
                    done
              containerName: ${APPLICATION_NAME}
        resources:
      triggers:
        - imageChangeParams:
            automatic: true
            containerNames:
              - ${APPLICATION_NAME}
            from:
              kind: ImageStreamTag
              name: ${APPLICATION_NAME}:latest
          type: ImageChange
        - type: ConfigChange
      replicas: "1"
      template:
        metadata:
          labels:
            app: ${APPLICATION_NAME}
            name: ${APPLICATION_NAME}-app
        spec:
          containers:
          - name: ${APPLICATION_NAME}
            image: ' '
            imagePullPolicy: IfNotPresent
            env:
            - name: SPRING_PROFILES_ACTIVE
              value: prod,swagger
            - name: SPRING_DATASOURCE_URL
              value: jdbc:mysql://${APPLICATION_NAME}-mysql/${MYSQL_DATABASE}
            - name: SPRING_DATASOURCE_USERNAME
              valueFrom:
                secretKeyRef:
                  name: ${APPLICATION_NAME}-mysql
                  key: database-user
            - name: SPRING_DATASOURCE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: ${APPLICATION_NAME}-mysql
                  key: database-password
            ports:
            - name: http
              containerPort: 8080
            readinessProbe:
              httpGet:
                path: /
                port: http
            livenessProbe:
              httpGet:
                path: /
                port: http
              initialDelaySeconds: 180
  -
    apiVersion: v1
    kind: Service
    metadata:
      name: ${APPLICATION_NAME}-app
      labels:
        app: ${APPLICATION_NAME}
    spec:
      ports:
        -
          name: http
          port: 8080
      selector:
        name: ${APPLICATION_NAME}-app
  -
    apiVersion: v1
    kind: Route
    metadata:
      name: ${APPLICATION_NAME}
      labels:
        app: ${APPLICATION_NAME}
    spec:
      to:
        kind: Service
        name: ${APPLICATION_NAME}-app
        weight: "100"
      port:
        targetPort: "http"
      wildcardPolicy: None
